Git Commands:

mkdir - make directory/folder
	ex. mkdir batch-165
cd - change directory
	ex. cd batch-165
pwd - print working directory
ls - list
touch - creating files
	ex. touch discussion.txt

mkdir d1 && cd d1

=====
SSH Key
=====

1. Open a Terminal
	Windows
		open the "Windows Terminal" or "Git Bash"
2. Create an SSH Key
		ssh-keygen
3. Copy the SSH Key
4. Configure the git account in the device/project
		a. Configure the global user email
			git config --global user.email "[git account email]"
			ex:
			git config --global user.email "enrique.o.legaspi@gmail.com"

		b. Configure the global user name
			git config --global user.name "[git account username]"
			ex:
			git config --global user.name "enrique legaspi"
5. Check the git user credentials
		git config --global --list

==============
Git Commands
==============
1. Initialize a local git repository
		git init
2. Peek at the states of the files/folders
		git status
3. Stage files in preparation for creating a commit
		staging files individually
			git add [filename]
			ex:
			git add discussion.txt

		staging all files
			git add .
			git add -A
4. Peek at the states of the files/folders
		git status
5. Create commit
		git commit -m "[message]"
		ex:
		git commit -m "Initial commit"
6. Peek at the states of the files/folders
		git status